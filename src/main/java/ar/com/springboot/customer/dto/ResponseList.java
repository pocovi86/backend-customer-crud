package ar.com.springboot.customer.dto;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ResponseList {
    private HttpStatus status;
    private String message;
    private List<CustomerDTO> data;

    public ResponseList() {
    }

    public ResponseList(HttpStatus status, String message, List<CustomerDTO> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CustomerDTO> getData() {
        return data;
    }

    public void setData(List<CustomerDTO> data) {
        this.data = data;
    }
}
