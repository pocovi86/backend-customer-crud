package ar.com.springboot.customer.dto;

import org.springframework.http.HttpStatus;

public class Response {
    private HttpStatus status;
    private String message;
    private CustomerDTO data;

    public Response() {
    }

    public Response(HttpStatus status, String message, CustomerDTO data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomerDTO getData() {
        return data;
    }

    public void setData(CustomerDTO data) {
        this.data = data;
    }
}
