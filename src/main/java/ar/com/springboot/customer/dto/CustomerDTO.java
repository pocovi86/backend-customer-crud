package ar.com.springboot.customer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class CustomerDTO implements Serializable {

    @JsonProperty(value = "id")
    private Integer id;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "lastName")
    private String lastName;

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "city")
    private CityDTO cityDTO;

    public CustomerDTO() {
    }

    public CustomerDTO(String name, String lastName, String phone, String email, CityDTO cityDTO) {
        this.name = name;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.cityDTO = cityDTO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }
}
