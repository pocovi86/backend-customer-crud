package ar.com.springboot.customer.controller;

import ar.com.springboot.customer.dto.CustomerDTO;
import ar.com.springboot.customer.dto.Response;
import ar.com.springboot.customer.dto.ResponseList;
import ar.com.springboot.customer.service.CustomerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @CacheEvict(value = "customerCache", allEntries = true)
    @GetMapping("/listClient")
    public ResponseList listCustomer() {
        List<CustomerDTO> dtoList = customerService.customers();
        return new ResponseList(HttpStatus.OK, "Customers List", dtoList);
    }

    @PostMapping("/create")
    public Response createCustomer(@RequestBody CustomerDTO dto) {
        if (StringUtils.isBlank(dto.getName())) {
            return new Response(HttpStatus.BAD_REQUEST, "The name is required", null);
        }
        if (StringUtils.isBlank(dto.getEmail())) {
            return new Response(HttpStatus.BAD_REQUEST, "The email is required", null);
        }
        if (StringUtils.isBlank(dto.getCityDTO().getCityName())){
            return new Response(HttpStatus.BAD_REQUEST, "The city name is required", null);
        }
        return new Response(HttpStatus.OK, "Customer saved with success!", customerService.save(dto));
    }

    @GetMapping("/detail/{id}")
    public Response getCustomerById(@PathVariable("id") int id) {
        if (!customerService.existById(id)){
            return new Response(HttpStatus.NOT_FOUND, "Customer with id does not exist: " + id, null);
        }
        return new Response(HttpStatus.OK, "Customer found", customerService.getOne(id));
    }

    @PutMapping("/update/{id}")
    public Response updateCustomer(@PathVariable("id") int id, @RequestBody CustomerDTO dto) {
        if (!customerService.existById(id)) {
            return new Response(HttpStatus.NOT_FOUND, "This client does not exist", null);
        }
        if (customerService.existByName(dto.getName()) && customerService.getByName(dto.getName()).getId() != id){
            return new Response(HttpStatus.NOT_FOUND, "This name client already exists", null);
        }
        return new Response(HttpStatus.OK,"Client update", customerService.update(id, dto));
    }

    @DeleteMapping("/delete/{id}")
    public Response deleteCustomer(@PathVariable("id") int id) {
        if (!customerService.existById(id)) {
            return new Response(HttpStatus.NOT_FOUND, "Customer with id does not exist: " + id, null);
        }
        customerService.delete(id);
        return new Response(HttpStatus.OK, "Client deleted", null);
    }
}
