package ar.com.springboot.customer.repositoy;

import ar.com.springboot.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    /**
     * busca por nombre si existe
     * @param name
     * @return true or false
     */
    boolean existsByName(String name);

    /**
     * busca por nombre y retorna un Customer
     * @param name
     * @return {@link Customer}
     */
    Optional<Customer> findByName(String name);
}
