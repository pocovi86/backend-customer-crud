package ar.com.springboot.customer.service.impl;

import ar.com.springboot.customer.dto.CustomerDTO;
import ar.com.springboot.customer.entity.Customer;
import ar.com.springboot.customer.repositoy.CustomerRepository;
import ar.com.springboot.customer.service.CustomerService;
import ar.com.springboot.customer.service.mapper.CustomerMapper;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.base.Preconditions;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    CustomerRepository respository;

    @Resource
    CustomerMapper customerMapper;

    @Override
    public List<CustomerDTO> customers() {
        log.debug("Solucitud para devolver uns lista de Customers");
        return respository.findAll().stream().map(customerMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional
    public CustomerDTO save(CustomerDTO customerDTO) {
        log.debug("Solicitud para crear un Customer: {}", customerDTO);
        Preconditions.checkArgument(customerDTO.getName() != null, "customer.error.name.null");
        return customerMapper.toDto(respository.save(customerMapper.toEntity(customerDTO)));
    }

    @Override
    public CustomerDTO update(Integer id, CustomerDTO customerDTO) {
        log.debug("Solicitud para obtener un Customer por id: {}", id);
        log.debug("Solicitud para update un Customer: {}", customerDTO);
        Preconditions.checkArgument(id != null, "id.error.null");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(customerDTO.getName()), "customerDTO.getName().null");

        Optional<Customer> oCustomer = respository.findById(id);
        if (!oCustomer.isPresent()) {
            log.error("Customer with id does not exist: " + id);
        }

        Customer customer = customerMapper.toEntity(customerDTO);
        customer.setId(oCustomer.get().getId());
        customer.getCity().setId(oCustomer.get().getCity().getId());
        return customerMapper.toDto(respository.save(customer));
    }

    @Override
    public CustomerDTO getOne(Integer id) {
        log.debug("Solicitud para obtener un customer por id: {}", id);
        Preconditions.checkArgument(id != null, "id.error.customer.null");
        return customerMapper.toDto(respository.getOne(id));
    }

    @Override
    public boolean existById(Integer id) {
        log.debug("Solicitud para verificar su existe el customer con el id: {}", id);
        Preconditions.checkArgument(id != null, "id.error.customer.null");
        return respository.existsById(id);
    }

    @Override
    public boolean existByName(String name) {
        log.debug("Solicitud para verificar un customer por el nombre: {}", name);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(name), "name.error.customer.null");
        return respository.existsByName(name);
    }

    @Override
    public CustomerDTO getByName(String name) {
        log.debug("Solicitud para obtener un customer por name: {}", name);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(name), "name.error.customer.null");
        return customerMapper.toDto(respository.findByName(name).get());
    }

    @Override
    public void delete(Integer id) {
        log.debug("Solicitud para eliminar un customer por el id: {}", id);
        Preconditions.checkArgument(id != null, "id.error.customer.null");
        respository.deleteById(id);
    }
}
