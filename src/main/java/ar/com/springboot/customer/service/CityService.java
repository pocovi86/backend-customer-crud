package ar.com.springboot.customer.service;

import ar.com.springboot.customer.entity.City;

import java.util.List;
import java.util.Optional;

public interface CityService {

    /**
     *
     * @param city
     */
    void save(City city);

    /**
     *
     * @return {@link List} of {@link City}
     */
    List<City> cityList();

    /**
     *
     * @param id
     * @return {@link Optional} of {@link City}
     */
    Optional<City> getOne(int id);

    /**
     *
     * @param id
     * @return true or false
     */
    boolean existById(int id);

    /**
     *
     * @param id
     * @return {@link Optional} of {@link City}
     */
    Optional<City> getCity(int id);
}
