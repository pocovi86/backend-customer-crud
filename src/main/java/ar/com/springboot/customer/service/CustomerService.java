package ar.com.springboot.customer.service;

import ar.com.springboot.customer.dto.CustomerDTO;
import ar.com.springboot.customer.entity.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

    /**
     *
     * @return {@link List} of {@link Customer}
     */
    List<CustomerDTO> customers();

    /**
     *
     * @param customer
     */
    CustomerDTO save(CustomerDTO customerDTO);

    /**
     *
     * @param id
     * @param customerDTO
     * @return {@link CustomerDTO}
     */
    CustomerDTO update(Integer id, CustomerDTO customerDTO);

    /**
     *
     * @param id
     * @return {@link CustomerDTO}
     */
    CustomerDTO getOne(Integer id);

    /**
     *
     * @param id
     * @return true or false
     */
    boolean existById(Integer id);

    /**
     *
     * @param name
     * @return true or false
     */
    boolean existByName(String name);

    /**
     *
     * @param name
     * @return {@link CustomerDTO}
     */
    CustomerDTO getByName(String name);

    /**
     *
     * @param id
     */
    void delete(Integer id);
}
