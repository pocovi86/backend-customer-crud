package ar.com.springboot.customer.service.impl;

import ar.com.springboot.customer.entity.City;
import ar.com.springboot.customer.repositoy.CityRepository;
import ar.com.springboot.customer.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CityServiceImpl implements CityService {
    @Autowired
    CityRepository repository;

    @Override
    public void save(City city) {
        repository.save(city);
    }

    @Override
    public List<City> cityList() {
        return repository.findAll();
    }

    @Override
    public Optional<City> getOne(int id) { return repository.findById(id); }

    @Override
    public boolean existById(int id) {
        return repository.existsById(id);
    }

    @Override
    public Optional<City> getCity(int id) {
        return repository.findById(id);
    }
}
