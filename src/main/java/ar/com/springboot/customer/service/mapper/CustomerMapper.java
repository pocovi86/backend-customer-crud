package ar.com.springboot.customer.service.mapper;

import ar.com.springboot.customer.dto.CustomerDTO;
import ar.com.springboot.customer.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    /**
     * @param customer
     * @return
     */

    @Mapping(source = "city", target = "cityDTO")
    CustomerDTO toDto(Customer customer);


    /**
     * @param customerDTO
     * @return
     */
    @Mapping(source = "cityDTO", target = "city")
    Customer toEntity(CustomerDTO customerDTO);
}
