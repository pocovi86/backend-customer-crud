package ar.com.springboot.customer.controller;

import ar.com.springboot.customer.CustomerApplication;
import ar.com.springboot.customer.dto.CityDTO;
import ar.com.springboot.customer.dto.CustomerDTO;
import ar.com.springboot.customer.entity.City;
import ar.com.springboot.customer.entity.Customer;
import ar.com.springboot.customer.repositoy.CityRepository;
import ar.com.springboot.customer.repositoy.CustomerRepository;
import ar.com.springboot.customer.rest.TestUtil;
import ar.com.springboot.customer.service.mapper.CustomerMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = CustomerApplication.class)
@AutoConfigureMockMvc
@WithMockUser
public class CustomerControllerTest {

    /*Customer*/
    private static final Integer DEFAULT_ID_CUSTOMER_T = 100;

    private static final Integer DEFAULT_ID_CUSTOMER = 1;

    private static final String DEFAULT_NAME = "AAAAAAAA";

    private static final String DEFAULT_LAST_NAME = "BBBBBBBB";

    private static final String DEFAULT_PHONE = "CCCCCCCC";

    private static final String DEFAULT_EMAIL = "DDDD@gmail.com";

    /*City*/
    private static final Integer DEFAULT_ID_CITY = 200;

    private static final String DEFAULT_NAME_CITY = "EEEEEEEE";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCustomerMockMvc;

    private CustomerDTO request;
    private CityDTO requestCity;
    private Customer customer;

    private CustomerMapper customerMapper;
    private City city;

    public static CustomerDTO createRequest(EntityManager em) {
        CustomerDTO request = new CustomerDTO();
        request.setName(DEFAULT_NAME);
        request.setLastName(DEFAULT_LAST_NAME);
        request.setPhone(DEFAULT_PHONE);
        request.setEmail(DEFAULT_EMAIL);
        return request;
    }

    public static CityDTO createRequestCity(EntityManager em) {
        CityDTO requestCity = new CityDTO();
        requestCity.setCityName(DEFAULT_NAME_CITY);
        return requestCity;
    }

    public static Customer createCustomerEntity(EntityManager em) {
        Customer customer = new Customer();
        customer.setName(DEFAULT_NAME);
        customer.setLastName(DEFAULT_LAST_NAME);
        customer.setPhone(DEFAULT_PHONE);
        customer.setEmail(DEFAULT_EMAIL);
        return customer;
    }

    public static City createCityEntity(EntityManager em) {
        City city = new City();
        city.setCityName(DEFAULT_NAME_CITY);
        return city;
    }

    @BeforeEach
    public void initTest() {
        requestCity = createRequestCity(em);
        request = createRequest(em);
        city = createCityEntity(em);
        customer = createCustomerEntity(em);

    }

    @Test
    @Transactional
    public void listCustomer() throws Exception {
        /*Initialize the database*/
        /*cityRepository.saveAndFlush(city);
        customer.setCity(city);*/
        customer = customerMapper.toEntity(request);

        customerRepository.saveAndFlush(customer);

        int cant = customerRepository.findAll().size();

        restCustomerMockMvc.perform(get("/api/listClient"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
        List<CustomerDTO> customerList = customerRepository.findAll().stream().map(customerMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
        assertThat(customerList).hasSize(cant);

    }

    @Test
    @Transactional
    public void createCustomer() throws Exception {
        /*Initialize the database*/
        customer.setCity(city);

        restCustomerMockMvc.perform(post("/api/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(customer)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("Customer saved with success!"));
    }

    @Test
    @Transactional
    public void createCustomerEmptyName() throws Exception {
        /*Initialize the database*/
        request.setName("");

        restCustomerMockMvc.perform(post("/api/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(request)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("The name is required"));
    }

    @Test
    @Transactional
    public void createCustomerEmptyEmail() throws Exception {
        /*Initialize the database*/
        request.setEmail("");

        restCustomerMockMvc.perform(post("/api/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(request)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("The email is required"));
    }

    @Test
    @Transactional
    public void createCustomerEmptyCityName() throws Exception {
        /*Initialize the database*/
        request.getCityDTO().setCityName("");

        restCustomerMockMvc.perform(post("/api/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(request)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("The city name is required"));

    }

    @Test
    @Transactional
    public void getCustomerById() throws Exception {
        /*Initialize the database*/
        cityRepository.saveAndFlush(city);
        customer.setCity(city);

        customerRepository.saveAndFlush(customer);

        restCustomerMockMvc.perform(get("/api/detail/{id}", customer.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(customer.getId()))
                .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
                .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
                .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
                .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
                .andExpect(jsonPath("$.city.id").value(city.getId()))
                .andExpect(jsonPath("$.city.cityName").value(DEFAULT_NAME_CITY));
    }

    @Test
    @Transactional
    public void getCustomerByIdEmpty() throws Exception {
        /*Initialize the database*/
        cityRepository.saveAndFlush(city);
        customer.setCity(city);
        customer.setId(DEFAULT_ID_CUSTOMER_T);
        customerRepository.saveAndFlush(customer);

        restCustomerMockMvc.perform(get("/api/detail/{id}", customer.getId()))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("This customer does not exist"));
    }

    @Test
    @Transactional
    public void updateCustomer() throws Exception {
        /*Initialize the database*/
        cityRepository.saveAndFlush(city);
        customer.setCity(city);
        customerRepository.saveAndFlush(customer);

        /*Customer customerUpdate = customerRepository.getOne(customer.getId());
        customerUpdate.setName(request.getName());
        customerUpdate.setLastName(request.getLastName());
        customerUpdate.setPhone(request.getPhone());
        customerUpdate.setEmail(request.getEmail());
        *//*int idCity = customerUpdate.getCity().getId();
        City city = cityRepository.getOne(idCity);
        city.setCityName(request.getCityName());
        customerUpdate.setCity(city);*//*

        customerRepository.saveAndFlush(customerUpdate);*/

        restCustomerMockMvc.perform(put("/api/update/{id}", customer.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(request)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("Client update"));
    }

    @Test
    @Transactional
    public void deleteCustomer() throws Exception {
        /*Initialize the database*/
        cityRepository.saveAndFlush(city);
        customer.setCity(city);

        customerRepository.saveAndFlush(customer);

        restCustomerMockMvc.perform(delete("/api/delete/{id}", customer.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("Client deleted"));
    }

    @Test
    @Transactional
    public void deleteCustomerByIdEmpty() throws Exception {
        /*Initialize the database*/
        cityRepository.saveAndFlush(city);
        customer.setCity(city);
        customer.setId(DEFAULT_ID_CUSTOMER_T);
        customerRepository.saveAndFlush(customer);

        restCustomerMockMvc.perform(delete("/api/delete/{id}", customer.getId()))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("This client does not exist"));
    }
}